# portfolio

Bitbucket url: https://bitbucket.org/cameronpadua/portfolio/src/master/

My main goal of this capstone was to create a 1-page website that could be
used for my portfolio. I wanted this site to minimal, clean, and flat. While
originally I wanted to do a dark theme, it was hard to make the colors contrast
without looking bad. Due to this, I switched to a light theme.

In addition to this, I wanted to stay close to my roots as a programmer. This
meant I wanted the site to have a "programmer environment feel". To do this,
I used a monospace font and styled it after a Python interpreter. I also made
sure to keep it brief since it is meant to be a portfolio to show a snapshot
of who I am as a person and Software Developer.

My inspirations for this project come a variety of places. Some of the inspirations
come from friends who are in IMD, while other come from professional designers. All
the places I took design inspirations from will be in the links below. My personal
favorites are Julian's and Katie Conforti's portfolio. These are the two I most likely
took the most inspiration from.

<ul>
  <li>www.kaiwa-projects.com</li>
  <li>http://www.creative-portfolios.com/portfolio_page/katie-conforti/</li>
  <li>http://www.creative-portfolios.com/portfolio_page/cassidy-williams/</li>
  <li>http://julianngo.me/</li>
  <li>https://www.rutujanehra.me/</li>
</ul>
