var welcomeMessage = ['Welcome!', 'My name is Cameron Padua.', 'I am a Software Developer......', 'for now.', 'Feel free to look around']
var speed = 70; /* The speed/duration of the effect in milliseconds */
var speedForward = 100,
  wait = 500,
  speedBackward = 35;
window.onload = function() {
  typeWriter("welcomeMsg", welcomeMessage);
};
$(document).ready(function() {
  var bullets = document.getElementsByTagName("span");
  for (bullet of bullets){
    bullet.textContent = "#" + bullet.textContent;
  }
});

window.onscroll = function() {
  scrollFunction()
};

function scrollFunction() {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
async function up() {
  while (document.body.scrollTop != 0 || document.documentElement.scrollTop != 0) {
    document.body.scrollTop -= 30; // Safari
    document.documentElement.scrollTop -= 30; // Everything else
    await sleep(.5);
  }
}

async function typeWriter(id, array) {
  var currentLetter = 0;
  var currentLine = 0;
  var line = document.getElementById(id);
  line.innerHTML = "";
  while (currentLine < array.length) {
    currentLetter = 0;
    while (currentLetter < array[currentLine].length) {
      line.innerHTML += array[currentLine].charAt(currentLetter);
      currentLetter++;
      await sleep(speedForward);
    }
    await sleep(wait);
    if (currentLine == array.length - 1) {
      break;
    }
    while (currentLetter >= 0) {
      line.innerHTML = line.textContent.slice(0, -1);
      currentLetter--;
      await sleep(speedBackward);
    }
    currentLine++;
  }
  await sleep(10000);
  typeWriter(id, array)
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
